use crate::card;

use bevy::prelude::*;
use rand::prelude::SliceRandom;
use rand::thread_rng;

pub struct DeckPlugin;

impl Plugin for DeckPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Deck::default())
            .add_systems(Startup, new_deck);
    }
}

#[derive(Resource, Debug)]
pub struct Deck {
    list: Vec<card::Card>,
}

impl Deck {
    pub fn draw(&mut self) -> Option<card::Card> {
        self.list.pop()
    }

    /// # Panics
    /// Will panic if it runs out of cards so it should only be called on a fresh deck
    pub fn new_hand(&mut self) -> Vec<card::Card> {
        let mut hand = vec![];
        for _ in 0..5 {
            let card = self.draw().expect("ran out of cards in deck!");
            hand.push(card);
        }
        hand
    }
}

fn new_deck(mut deck: ResMut<Deck>) {
    let my_deck = Deck::default();
    let mut deck_arr = my_deck.list;
    deck_arr.shuffle(&mut thread_rng());
    deck.list = deck_arr;
}

impl Default for Deck {
    /// Makes an unsorted deck of 30 with 5 of each card type
    fn default() -> Self {
        use card::Card;
        use card::CardType;
        Deck {
            list: vec![
                Card::new(CardType::Star),
                Card::new(CardType::Star),
                Card::new(CardType::Star),
                Card::new(CardType::Star),
                Card::new(CardType::Star),
                Card::new(CardType::King),
                Card::new(CardType::King),
                Card::new(CardType::King),
                Card::new(CardType::King),
                Card::new(CardType::King),
                Card::new(CardType::Queen),
                Card::new(CardType::Queen),
                Card::new(CardType::Queen),
                Card::new(CardType::Queen),
                Card::new(CardType::Queen),
                Card::new(CardType::Rose),
                Card::new(CardType::Rose),
                Card::new(CardType::Rose),
                Card::new(CardType::Rose),
                Card::new(CardType::Rose),
                Card::new(CardType::Duck),
                Card::new(CardType::Duck),
                Card::new(CardType::Duck),
                Card::new(CardType::Duck),
                Card::new(CardType::Duck),
                Card::new(CardType::Cloud),
                Card::new(CardType::Cloud),
                Card::new(CardType::Cloud),
                Card::new(CardType::Cloud),
                Card::new(CardType::Cloud),
            ],
        }
    }
}
