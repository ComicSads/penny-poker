#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash)]
pub enum CardType {
    Star,
    King,
    Queen,
    Rose,
    Duck,
    Cloud,
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash)]
pub struct Card {
    face: CardType,
}

impl Card {
    pub fn new(face: CardType) -> Self {
        Card { face }
    }
}
