#![allow(dead_code)]

use bevy::prelude::*;

mod card;
mod deck;

use deck::DeckPlugin;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(DeckPlugin)
        .add_systems(Update, print_hand_to_cli)
        .run();
}

fn print_hand_to_cli(mut deck: ResMut<deck::Deck>, input: Res<Input<KeyCode>>) {
    if input.just_pressed(KeyCode::H) {
        let hand = deck.new_hand();
        println!("{:?}", hand);
    }
}
